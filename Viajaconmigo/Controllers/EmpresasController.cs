﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viajaconmigo.Models;

namespace Viajaconmigo.Controllers
{
    public class EmpresasController : Controller
    {
        private viajaconmigoEntities db = new viajaconmigoEntities();

        // GET: Empresas
        public ActionResult Index()
        {
            return View(db.Empresa.ToList());
        }
        public ActionResult alianza()
        {

            return View();
        }
        public ActionResult arauca()
        {

            return View();
        }
        public ActionResult Armenia()
        {

            return View();

        }
        public ActionResult Autolegal()
        {

            return View();

        }
        public ActionResult Autolujo()
        {

            return View();

        }
        public ActionResult Bolivariano()
        {

            return View();

        }
        public ActionResult Brasilia()
        {

            return View();

        }
        public ActionResult Cafetero()
        {

            return View();

        }
        public ActionResult Coopuertos()
        {

            return View();

        }
        public ActionResult Cootraman()
        {

            return View();

        }
        public ActionResult Cootranshuila()
        {

            return View();

        }
        public ActionResult Cootransnorte()
        {

            return View();

        }
        public ActionResult Contransrio()
        {

            return View();

        }
        public ActionResult Copetran()
        {

            return View();

        }
        public ActionResult Cotracol()
        {

            return View();

        }
        public ActionResult Contranshuila()
        {

            return View();

        }
        public ActionResult Flotaoccidental()
        {

            return View();

        }
        public ActionResult Flotaospina()
        {

            return View();

        }
        public ActionResult Norcaldas()
        {

            return View();

        }
        public ActionResult Palmira()
        {

            return View();

        }
        public ActionResult Sideral()
        {

            return View();

        }
        public ActionResult Taxlaferia()
        {

            return View();

        }
        public ActionResult Tolima()
        {

            return View();

        }
        public ActionResult Transarmenia()
        {

            return View();

        }
        public ActionResult Transporteirra()
        {

            return View();

        }
        public ActionResult Trejos()
        {

            return View();

        }
        // GET: Empresas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empresa empresa = db.Empresa.Find(id);
            if (empresa == null)
            {
                return HttpNotFound();
            }
            return View(empresa);
        }

        // GET: Empresas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Empresas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NitEmpresa,Nombre,Telefono")] Empresa empresa)
        {
            if (ModelState.IsValid)
            {
                db.Empresa.Add(empresa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(empresa);
        }

        // GET: Empresas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empresa empresa = db.Empresa.Find(id);
            if (empresa == null)
            {
                return HttpNotFound();
            }
            return View(empresa);
        }

        // POST: Empresas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "NitEmpresa,Nombre,Telefono")] Empresa empresa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(empresa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(empresa);
        }

        // GET: Empresas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empresa empresa = db.Empresa.Find(id);
            if (empresa == null)
            {
                return HttpNotFound();
            }
            return View(empresa);
        }

        // POST: Empresas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Empresa empresa = db.Empresa.Find(id);
            db.Empresa.Remove(empresa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
