﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viajaconmigo.Models;

namespace Viajaconmigo.Controllers
{
    public class Vehiculo_RutaController : Controller
    {
        private viajaconmigoEntities db = new viajaconmigoEntities();

        // GET: Vehiculo_Ruta
        public ActionResult Index()
        {
            var vehiculo_Ruta = db.Vehiculo_Ruta.Include(v => v.Rutas).Include(v => v.Vehiculo);
            return View(vehiculo_Ruta.ToList());
        }

        // GET: Vehiculo_Ruta/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo_Ruta vehiculo_Ruta = db.Vehiculo_Ruta.Find(id);
            if (vehiculo_Ruta == null)
            {
                return HttpNotFound();
            }
            return View(vehiculo_Ruta);
        }

        // GET: Vehiculo_Ruta/Create
        public ActionResult Create()
        {
            ViewBag.IdRutas = new SelectList(db.Rutas, "IdRutas", "IdRutas");
            ViewBag.Placa = new SelectList(db.Vehiculo, "Placa", "Placa");
            return View();
        }

        // POST: Vehiculo_Ruta/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Placa,IdRutas,Cupos")] Vehiculo_Ruta vehiculo_Ruta)
        {
            if (ModelState.IsValid)
            {
                db.Vehiculo_Ruta.Add(vehiculo_Ruta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdRutas = new SelectList(db.Rutas, "IdRutas", "Origen", vehiculo_Ruta.IdRutas);
            ViewBag.Placa = new SelectList(db.Vehiculo, "Placa", "Modelo", vehiculo_Ruta.Placa);
            return View(vehiculo_Ruta);
        }

        // GET: Vehiculo_Ruta/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo_Ruta vehiculo_Ruta = db.Vehiculo_Ruta.Find(id);
            if (vehiculo_Ruta == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdRutas = new SelectList(db.Rutas, "IdRutas", "Origen", vehiculo_Ruta.IdRutas);
            ViewBag.Placa = new SelectList(db.Vehiculo, "Placa", "Modelo", vehiculo_Ruta.Placa);
            return View(vehiculo_Ruta);
        }

        // POST: Vehiculo_Ruta/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Placa,IdRutas")] Vehiculo_Ruta vehiculo_Ruta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehiculo_Ruta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdRutas = new SelectList(db.Rutas, "IdRutas", "Origen", vehiculo_Ruta.IdRutas);
            ViewBag.Placa = new SelectList(db.Vehiculo, "Placa", "Modelo", vehiculo_Ruta.Placa);
            return View(vehiculo_Ruta);
        }

        // GET: Vehiculo_Ruta/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vehiculo_Ruta vehiculo_Ruta = db.Vehiculo_Ruta.Find(id);
            if (vehiculo_Ruta == null)
            {
                return HttpNotFound();
            }
            return View(vehiculo_Ruta);
        }

        // POST: Vehiculo_Ruta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vehiculo_Ruta vehiculo_Ruta = db.Vehiculo_Ruta.Find(id);
            db.Vehiculo_Ruta.Remove(vehiculo_Ruta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
