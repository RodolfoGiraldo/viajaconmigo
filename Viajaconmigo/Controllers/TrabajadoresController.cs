﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viajaconmigo.Models;

namespace Viajaconmigo.Controllers
{
    public class TrabajadoresController : Controller
    {
        private static string correo, contraseña, tipoDeUsuario;
        private viajaconmigoEntities db = new viajaconmigoEntities();

        // GET: Trabajadores
        public ActionResult Index()
        {
            var trabajadores = db.Trabajadores.Include(t => t.Empresa);
            return View(trabajadores.ToList());
        }

        // GET: Trabajadores/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trabajadores trabajadores = db.Trabajadores.Find(id);
            if (trabajadores == null)
            {
                return HttpNotFound();
            }
            return View(trabajadores);
        }
        public void GuardarLogin()
        {
            Login login = new Login();
            login.Contraseña = contraseña;
            login.Usuario = correo;

            if (tipoDeUsuario == "Administrador")
            {
                login.TipoUsuario = "2";
            }
            else if (tipoDeUsuario == "Usuario")
            {
                login.TipoUsuario = "3";
            }
            if(tipoDeUsuario == "Conductor")
            {
                login.TipoUsuario = "4";
            }


            db.Login.Add(login);
            db.SaveChanges();
        }
        // GET: Trabajadores/Create
        public ActionResult Create()
        {
            ViewBag.NitEmpresa = new SelectList(db.Empresa, "NitEmpresa", "Nombre");
            return View();
        }

        // POST: Trabajadores/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CodigoTrabajor,NitEmpresa,Cedula,Nombre,Apellido,Telefono,Direccion,Tipo,Correo,Password")] Trabajadores trabajadores)
        {
            if (ModelState.IsValid)
            {
                
                db.Trabajadores.Add(trabajadores);
                db.SaveChanges();

                correo = trabajadores.Correo;
                contraseña = trabajadores.Password;
                tipoDeUsuario = trabajadores.Tipo;


                GuardarLogin();
                return RedirectToAction("Index");
            }

            ViewBag.NitEmpresa = new SelectList(db.Empresa, "NitEmpresa", "Nombre", trabajadores.NitEmpresa);
            return View(trabajadores);
        }

        // GET: Trabajadores/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trabajadores trabajadores = db.Trabajadores.Find(id);
            if (trabajadores == null)
            {
                return HttpNotFound();
            }
            ViewBag.NitEmpresa = new SelectList(db.Empresa, "NitEmpresa", "Nombre", trabajadores.NitEmpresa);
            return View(trabajadores);
        }

        // POST: Trabajadores/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CodigoTrabajor,NitEmpresa,Cedula,Nombre,Apellido,Telefono,Direccion,Tipo,Correo,Password")] Trabajadores trabajadores)
        {
            if (ModelState.IsValid)
            {
                db.Entry(trabajadores).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.NitEmpresa = new SelectList(db.Empresa, "NitEmpresa", "Nombre", trabajadores.NitEmpresa);
            return View(trabajadores);
        }

        // GET: Trabajadores/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trabajadores trabajadores = db.Trabajadores.Find(id);
            if (trabajadores == null)
            {
                return HttpNotFound();
            }
            return View(trabajadores);
        }

        // POST: Trabajadores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Trabajadores trabajadores = db.Trabajadores.Find(id);
            db.Trabajadores.Remove(trabajadores);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
