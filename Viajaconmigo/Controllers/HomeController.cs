﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viajaconmigo.Models;


namespace Viajaconmigo.Controllers
{
    public class HomeController : Controller
    {
        private int puestosDisponibles = 0;
        private viajaconmigoEntities db = new viajaconmigoEntities();
        public ActionResult Index()
        {
            ViewBag.Destino = new SelectList(db.Rutas, "Destino", "Destino");
            return View();
        }

        public ActionResult About()
        {

            return View();
        }

        public ActionResult Contact()
        {

            return View();
        }
        public ActionResult Logeo()
        {

            return View();
        }
        public ActionResult Conductores()
        {

            return View();
        }
        public ActionResult Dml()
        {

            return View();
        }
        public ActionResult Estadisticadeventa()
        {

            return View();
        }
        public ActionResult Formadepago()
        {

            return View();
        }
        public ActionResult IngresoAdministrador()
        {

            return View();
        }
        public ActionResult IngresoUsuario()
        {

            return View();
        }
        public ActionResult Rutas()
        {

            return View();
        }
        public ActionResult Vehiculos()
        {

            return View();
        }
        public ActionResult Consultas()
        {

            return View();
        }
        public ActionResult PagoAdministrativo()
        {

            return View();
        }
        public ActionResult Registrarse()
        {

            return View();
        }
        public ActionResult InicioAdministrador()
        {

            return View();
        }

        public ActionResult Pago()
        {
            ViewBag.Cedula_Cliente = new SelectList(db.Cliente, "Cedula", "Cedula");
            ViewBag.Id_Conductor = new SelectList(db.Trabajadores, "CodigoTrabajor", "CodigoTrabajor");
            ViewBag.Id_Despachador = new SelectList(db.Trabajadores, "CodigoTrabajor", "CodigoTrabajor");
            ViewBag.Id_Vehiculo_Ruta = new SelectList(db.Vehiculo_Ruta, "Id", "Id");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Pago([Bind(Include = "Id_Venta,Cedula_Cliente,Id_Vehiculo_Ruta,Id_Conductor,Id_Despachador,Puestos")] Ventas ventas)
        {
            puestosDisponibles = restarCupos(ventas.Id_Vehiculo_Ruta, ventas.Puestos);

            if (ModelState.IsValid && puestosDisponibles > 0)
            {
                db.Ventas.Add(ventas);
                GuardarReporte(ventas.TotalPrecio, ventas.Puestos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Cedula_Cliente = new SelectList(db.Cliente, "Cedula", "Cedula", ventas.Cedula_Cliente);
            ViewBag.Id_Conductor = new SelectList(db.Trabajadores, "CodigoTrabajor", "CodigoTrabajor", ventas.Id_Conductor);
            ViewBag.Id_Despachador = new SelectList(db.Trabajadores, "CodigoTrabajor", "CodigoTrabajor", ventas.Id_Despachador);
            ViewBag.Id_Vehiculo_Ruta = new SelectList(db.Vehiculo_Ruta, "Id", "Id", ventas.Id_Vehiculo_Ruta);
            return View(ventas);

        }

        public int restarCupos(int Id_Vehiculo_Ruta, int puestos)
        {
            Vehiculo_Ruta vehiculo_Ruta = db.Vehiculo_Ruta.Find(Id_Vehiculo_Ruta);
            vehiculo_Ruta.Cupos = vehiculo_Ruta.Cupos - puestos;
            db.Entry(vehiculo_Ruta).State = EntityState.Modified;

            return vehiculo_Ruta.Cupos;
        }

        public void GuardarReporte(int TotalDinero, int Puestos)
        {
            Reporte reporte = db.Reporte.Find(1);

            reporte.NumeroVentas += 1;
            reporte.TiquetesVendidos += Puestos;
            reporte.TotalDinero += TotalDinero;

            db.Entry(reporte).State = EntityState.Modified;

        }

        public ActionResult Inicio()
        {
            ViewBag.Destino = new SelectList(db.Rutas, "Destino", "Destino");
            return View();
        }




        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empresa empresa = db.Empresa.Find(id);
            if (empresa == null)
            {
                return HttpNotFound();
            }
            return View(empresa);
        }

        // GET: Empresas/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: Empresas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "Destino")] Rutas rutas)
        {
            if (rutas.Destino != null)
            {
                return RedirectToAction("https://localhost:44321/Rutas/SearchByDestino/?destino=" + rutas.Destino);
            }
            return RedirectToAction("Index");
        }

        // GET: Empresas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empresa empresa = db.Empresa.Find(id);
            if (empresa == null)
            {
                return HttpNotFound();
            }
            return View(empresa);
        }

        // POST: Empresas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "NitEmpresa,Nombre,Telefono")] Empresa empresa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(empresa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(empresa);
        }

        // GET: Empresas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Empresa empresa = db.Empresa.Find(id);
            if (empresa == null)
            {
                return HttpNotFound();
            }
            return View(empresa);
        }

        // POST: Empresas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Empresa empresa = db.Empresa.Find(id);
            db.Empresa.Remove(empresa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}