﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Viajaconmigo.Models;

namespace Viajaconmigo.Controllers
{
    public class VentasController : Controller
    {
        private int puestosDisponibles;
        private viajaconmigoEntities db = new viajaconmigoEntities();

        // GET: Ventas
        public ActionResult Index()
        {
            var ventas = db.Ventas.Include(v => v.Cliente).Include(v => v.Trabajadores).Include(v => v.Trabajadores1).Include(v => v.Vehiculo_Ruta);
            return View(ventas.ToList());
        }

        public ActionResult VerAdmin()
        {
            var ventas = db.Ventas.Include(v => v.Cliente).Include(v => v.Trabajadores).Include(v => v.Trabajadores1).Include(v => v.Vehiculo_Ruta);
            return View(ventas.ToList());
        }
        public ActionResult Reporte()
        {

            return View();
        }

        // GET: Ventas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ventas ventas = db.Ventas.Find(id);
            if (ventas == null)
            {
                return HttpNotFound();
            }
            return View(ventas);
        }

        // GET: Ventas/Create
        public ActionResult Create()
        {
            ViewBag.Cedula_Cliente = new SelectList(db.Cliente, "Cedula", "Cedula");
            ViewBag.Id_Conductor = new SelectList(db.Trabajadores, "CodigoTrabajor", "CodigoTrabajor");
            ViewBag.Id_Despachador = new SelectList(db.Trabajadores, "CodigoTrabajor", "CodigoTrabajor");
            ViewBag.Id_Vehiculo_Ruta = new SelectList(db.Vehiculo_Ruta, "Id", "Id");
            return View();
        }

        // POST: Ventas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Venta,Cedula_Cliente,Id_Vehiculo_Ruta,Id_Conductor,Id_Despachador,Puestos")] Ventas ventas)
        {
            puestosDisponibles = restarCupos(ventas.Id_Vehiculo_Ruta, ventas.Puestos);
            Vehiculo_Ruta vehiculo_Ruta = db.Vehiculo_Ruta.Find(ventas.Id_Vehiculo_Ruta);





            if (ModelState.IsValid && puestosDisponibles > 0)
            {
                ventas.TotalPrecio = ventas.Puestos * vehiculo_Ruta.Rutas.Precio;
                db.Ventas.Add(ventas);
                db.SaveChanges();

                GuardarReporte(ventas.TotalPrecio, ventas.Puestos);
                return RedirectToAction("Index");
            }

            ViewBag.Cedula_Cliente = new SelectList(db.Cliente, "Cedula", "Nombre", ventas.Cedula_Cliente);
            ViewBag.Id_Conductor = new SelectList(db.Trabajadores, "CodigoTrabajor", "Nombre", ventas.Id_Conductor);
            ViewBag.Id_Despachador = new SelectList(db.Trabajadores, "CodigoTrabajor", "Nombre", ventas.Id_Despachador);
            ViewBag.Id_Vehiculo_Ruta = new SelectList(db.Vehiculo_Ruta, "Id", "Placa", ventas.Id_Vehiculo_Ruta);
            return View(ventas);

        }

        public void GuardarReporte(int TotalDinero, int Puestos)
        {
            Reporte reporte = db.Reporte.Find(1);

            reporte.NumeroVentas = reporte.NumeroVentas + 1;
            reporte.TiquetesVendidos = Puestos + reporte.TiquetesVendidos;
            reporte.TotalDinero = TotalDinero + reporte.TotalDinero;
            db.Entry(reporte).State = EntityState.Modified;
            db.SaveChanges();
        }

        public int restarCupos(int Id_Vehiculo_Ruta, int puestos)
        {
            Vehiculo_Ruta vehiculo_Ruta = db.Vehiculo_Ruta.Find(Id_Vehiculo_Ruta);
            vehiculo_Ruta.Cupos = vehiculo_Ruta.Cupos - puestos;
            db.Entry(vehiculo_Ruta).State = EntityState.Modified;

            return vehiculo_Ruta.Cupos;
        }

        // GET: Ventas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ventas ventas = db.Ventas.Find(id);
            if (ventas == null)
            {
                return HttpNotFound();
            }
            ViewBag.Cedula_Cliente = new SelectList(db.Cliente, "Cedula", "Cedula", ventas.Cedula_Cliente);
            ViewBag.Id_Conductor = new SelectList(db.Trabajadores, "CodigoTrabajor", "CodigoTrabajor", ventas.Id_Conductor);
            ViewBag.Id_Despachador = new SelectList(db.Trabajadores, "CodigoTrabajor", "CodigoTrabajor", ventas.Id_Despachador);
            ViewBag.Id_Vehiculo_Ruta = new SelectList(db.Vehiculo_Ruta, "Id", "Id", ventas.Id_Vehiculo_Ruta);
            return View(ventas);
        }

        // POST: Ventas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Venta,Cedula_Cliente,Id_Vehiculo_Ruta,Id_Conductor,Id_Despachador")] Ventas ventas)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ventas).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Cedula_Cliente = new SelectList(db.Cliente, "Cedula", "Nombre", ventas.Cedula_Cliente);
            ViewBag.Id_Conductor = new SelectList(db.Trabajadores, "CodigoTrabajor", "Nombre", ventas.Id_Conductor);
            ViewBag.Id_Despachador = new SelectList(db.Trabajadores, "CodigoTrabajor", "Nombre", ventas.Id_Despachador);
            ViewBag.Id_Vehiculo_Ruta = new SelectList(db.Vehiculo_Ruta, "Id", "Placa", ventas.Id_Vehiculo_Ruta);
            return View(ventas);
        }

        // GET: Ventas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ventas ventas = db.Ventas.Find(id);
            if (ventas == null)
            {
                return HttpNotFound();
            }
            return View(ventas);
        }

        // POST: Ventas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ventas ventas = db.Ventas.Find(id);
            db.Ventas.Remove(ventas);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
